<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"));
while ($ar_sales = $db_sales->Fetch())
{
    $email = stristr($ar_sales["USER_EMAIL"], '@');
    $arEmail[]= $email;
}

$arResult['emails'] = array_count_values($arEmail);
$this->IncludeComponentTemplate();
?>